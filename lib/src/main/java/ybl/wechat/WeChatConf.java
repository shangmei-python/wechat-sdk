package ybl.wechat;

public class WeChatConf {
    private WeChat WECHAT;

    public static class WeChat {
        private String APP_ID;
        private String APP_SECRET;
        private String OPEN_API_URL_PREFIX;
        private String TOKEN;

        public String getAPP_ID() {
            return APP_ID;
        }

        public void setAPP_ID(String APP_ID) {
            this.APP_ID = APP_ID;
        }

        public String getAPP_SECRET() {
            return APP_SECRET;
        }

        public void setAPP_SECRET(String APP_SECRET) {
            this.APP_SECRET = APP_SECRET;
        }

        public String getOPEN_API_URL_PREFIX() {
            return OPEN_API_URL_PREFIX;
        }

        public void setOPEN_API_URL_PREFIX(String OPEN_API_URL_PREFIX) {
            this.OPEN_API_URL_PREFIX = OPEN_API_URL_PREFIX;
        }

        public String getTOKEN() {
            return TOKEN;
        }

        public void setTOKEN(String tOKEN) {
            TOKEN = tOKEN;
        }

    }

    public WeChat getWECHAT() {
        return WECHAT;
    }

    public void setWECHAT(WeChat WECHAT) {
        this.WECHAT = WECHAT;
    }
}
