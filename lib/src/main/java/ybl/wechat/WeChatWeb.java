package ybl.wechat;

import org.json.JSONObject;

public class WeChatWeb {
    private static final WeChatConf WE_CHAT_CONF = ConfigSingleton.getInstance();

    /**
     * 获取access_token。
     *
     * @param code 授权码
     * @return access_token的JSONObject表示
     */
    public static JSONObject getAccessToken(String code) {
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token?" +
                "appid=" + WE_CHAT_CONF.getWECHAT().getAPP_ID() +
                "&secret=" + WE_CHAT_CONF.getWECHAT().getAPP_SECRET() +
                "&code=" + code +
                "&grant_type=authorization_code";

        return WeChatUtils.executeRequest(url);
    }

    /**
     * 刷新access_token。
     *
     * @param refresh_token 刷新令牌
     * @return 刷新后的access_token的JSONObject表示
     */
    public static JSONObject refreshAccessToken(String refresh_token) {
        String url = "https://api.weixin.qq.com/sns/oauth2/refresh_token?" +
                "appid=" + WE_CHAT_CONF.getWECHAT().getAPP_ID() +
                "&grant_type=refresh_token" +
                "&refresh_token=" + refresh_token;

        return WeChatUtils.executeRequest(url);
    }

    /**
     * 获取用户信息。
     *
     * @param accessToken 访问令牌
     * @param openid      用户的唯一标识
     * @return 用户信息的JSONObject表示
     */
    public static JSONObject getUserInfo(String accessToken, String openid) {
        String url = "https://api.weixin.qq.com/sns/userinfo?" +
                "access_token=" + accessToken +
                "&openid=" + openid +
                "&lang=zh_CN";

        return WeChatUtils.executeRequest(url);
    }
}
