package ybl.wechat;

public class ApiException extends RuntimeException {
    public ApiException(String message) {
        super(message);
    }
}
