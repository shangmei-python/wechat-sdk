package ybl.wechat.entities;

import java.sql.Timestamp;

public class AccessToken {
    private String accessToken;
    private Timestamp updateAt;

    // 无参构造函数
    public AccessToken() {
        // 这个构造函数可能什么也不做，只是为了满足JDBI等库的要求
    }

    // 带所有属性的构造函数
    public AccessToken(String accessToken, Timestamp updateAt) {
        this.accessToken = accessToken;
        this.updateAt = updateAt;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Timestamp getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Timestamp updateAt) {
        this.updateAt = updateAt;
    }

    @Override
    public String toString() {
        return "AccessToken [accessToken=" + accessToken + ", updateAt=" + updateAt + "]";
    }

}
