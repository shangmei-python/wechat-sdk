package ybl.wechat.entities;

import java.sql.Timestamp;

public class QrCode {
    private String ticket;
    private int expire_seconds;
    private Timestamp createdAt;
    private boolean flag;

    public QrCode() {
    }

    public QrCode(String ticket, int expire_seconds, Timestamp createdAt, boolean flag) {
        this.ticket = ticket;
        this.expire_seconds = expire_seconds;
        this.createdAt = createdAt;
        this.flag = flag;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public int getExpire_seconds() {
        return expire_seconds;
    }

    public void setExpire_seconds(int expire_seconds) {
        this.expire_seconds = expire_seconds;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "QrCode [ticket=" + ticket + ", expire_seconds=" + expire_seconds + ", createdAt=" + createdAt
                + ", flag=" + flag + "]";
    }

}
